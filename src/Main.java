import edu.kit.informatik.Terminal;

/**
 * Diese Klasse behandelt die Nutzerinteraktionen. Dabei wird die Terminal Klasse verwendet um zum Einen Eingaben
 * über die Konsole einzulesen und zum Anderen um die entsprechenden Ergebnisse wieder auf der Konsole auszugeben.
 * Als temporärer Speicher wird dabei eine SortedList verwendet.
 *
 * @author Cedrico Knoesel
 * @since 18.12.2018
 */
public class Main {

    public static void main(String[] args) {
        boolean running = true;
        SortedList sortedList = new SortedList();
        while (running) {
            String input = Terminal.readLine();
            String[] command = input.split(" ");
            switch (command[0]) {
                case "add":
                    sortedList.add(Integer.parseInt(command[1]));
                    break;
                case "print":
                    Terminal.printLine(sortedList.print());
                    break;
                case "size":
                    Terminal.printLine(sortedList.size());
                    break;
                case "isEmpty":
                    Terminal.printLine(sortedList.isEmpty());
                    break;
                case "clear":
                    sortedList.clear();
                    break;
                case "get":
                    Terminal.printLine(sortedList.get(Integer.parseInt(command[1])));
                    break;
                case "indexOf":
                    Terminal.printLine(sortedList.indexOf(Integer.parseInt(command[1])));
                    break;
                case "lastIndexOf":
                    Terminal.printLine(sortedList.lastIndexOf(Integer.parseInt(command[1])));
                    break;
                case "remove":
                    Terminal.printLine(sortedList.remove(Integer.parseInt(command[1])));
                    break;
                case "contains":
                    Terminal.printLine(sortedList.contains(Integer.parseInt(command[1])));
                    break;
                case "quit":
                    running = false;
                    break;
            }
        }
    }
}
