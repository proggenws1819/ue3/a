/**
 * Diese Klasse implementiert nach dem Entwurfsmuster Kompositum eine sortierte einfach verkette Liste von Integer.
 * Außerdem besitzt die Klasse alle notwendigen Methoden zur Erstellen/Bearbeiten/Ausgeben der Liste.
 *
 * @author Cedrico Knoesel
 * @since 18.12.2018
 */
public class SortedList {
    private ListElementInterface head;

    public SortedList() {
        this.head = new NullListElement();
    }

    /**
     * Fügt einen neuen Integer der Liste sortiert hinzu
     *
     * @param number hinzuzufügender Integer
     */
    public void add(int number) {
        this.head = this.head.add(number);
    }

    /**
     * Gibt die Werte der Liste in einem festgelegten Format als String zurück.
     *
     * @return Ein String der alle Elemente der Liste nach folgendem Format enthält: [int, int, ...]
     */
    public String print() {
        StringBuilder stringBuilder = this.head.print(new StringBuilder("["));
        return stringBuilder.toString();
    }

    /**
     * Gibt die Anzahl der Elemente der Liste zurück.
     *
     * @return Anzahl der Elemente
     */
    public int size() {
        return head.size();
    }

    /**
     * Wertet aus, ob Liste leer ist.
     *
     * @return true, falls Liste leer ist, false andernfallse
     */
    public boolean isEmpty() {
        return this.head.isEmpty();
    }

    /**
     * Löscht alle Elemente der Liste
     */
    public void clear() {
        this.head = new NullListElement();
    }

    /**
     * Gibt den Wert des Elements an der Stelle der Liste mit dem übergebenen Index zurück.
     *
     * @param index Index des gesuchten Elements der Liste
     * @return Wert des Elements an der Stelle Index.
     */
    public int get(int index) {
        return this.head.get(index, 0);
    }

    /**
     * Durchsucht die Liste nach dem ersten Vorkommen eines Integer Wertes und gibt, falls vorhanden den entsprechenden
     * Index zurück
     *
     * @param number Integer Wert des zu suchenden Elements
     * @return Index des gefundenen Elements, falls vorhanden, andernfalls -1
     */
    public int indexOf(int number) {
        return this.head.indexOf(number, 0);
    }

    /**
     * Durchsucht die Liste nach dem letzten Vorkommen eines Integer Wertes und gibt, falls vorhanden den
     * entsprechenden Index zurück
     *
     * @param number Integer Wert des zu suchenden Elements
     * @return letzter Index des gefundenen Elements, falls vorhanden, andernfalls -1
     */
    public int lastIndexOf(int number) {
        return this.head.lastIndexOf(number, 0, -1);
    }

    /**
     * Entfernt das erste Auftreten der angegebenen Zahl aus der Liste und gibt Erfolgswert des Löschvorgangs zurück.
     * Letzterer wird zunächst über die contains-Methode bestimmt.
     *
     * @param number Die zu suchende Zahl
     * @return true, falls Löschvorgang erfolgreich, false, anderfalls
     */
    public boolean remove(int number) {
        boolean contains = this.contains(number);
        this.head = this.head.remove(number);
        return contains;
    }

    /**
     * Durchsucht die Liste nach der angegebene Zahl und gibt an, ob sie mindestens einmal vorkommt
     *
     * @param number Die zu suchende Zahl
     * @return true, falls Zahl mindestens einmal vorhanden, false, anderfalls
     */
    public boolean contains(int number) {
        return this.head.contains(number);
    }
}
