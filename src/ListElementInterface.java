/**
 * Dieses Interface beschreibt alle notwendigen Methoden eines Elements einer Liste.
 *
 * @author Cedrico Knoesel
 * @since 18.12.2018
 */
interface ListElementInterface {
    ListElementInterface add(int number);

    StringBuilder print(StringBuilder stringBuilder);

    int size();

    boolean isEmpty();

    int get(int index, int counter);

    int indexOf(int number, int counter);

    int lastIndexOf(int number, int counter, int currentFoundIndex);

    ListElementInterface remove(int number);

    boolean contains(int number);
}
