/**
 * Diese Klasse implementiert ein Element aus der Liste mit tatsächlichen Inhalt. Das bedeutet jedes Objekt dieser
 * Klasse hat sicher einen zugeordneten Integer als Wert und einen Nachfolger.
 *
 * @author Cedrico Knoesel
 * @since 18.12.2018
 */
class ListElement implements ListElementInterface {
    private ListElementInterface next;
    private int number;

    ListElement(int number, ListElementInterface next) {
        this.number = number;
        this.next = next;
    }

    /**
     * Fügt ein neues ListElement als Vorgänger ein, falls der neue Wert kleiner als der momentane ist.
     * Andernfalls wird der neue Wert rekursiv an den Nachfolger zum Einfügen übergeben.
     *
     * @param number Neuer Wert
     * @return Neuer Nachfolger
     */
    @Override
    public ListElementInterface add(int number) {
        if (this.number > number) {
            return new ListElement(number, this);
        } else {
            this.next = next.add(number);
            return this;
        }
    }

    /**
     * Baut rekursiv einen Ausgabestring auf, indem der aktuelle Wert und das Zeichen ',' hinzugefügt werden
     *
     * @param stringBuilder Bisheriger aufgebauter StringBuilder
     * @return Fertig aufgebauter StringBuilder
     */
    @Override
    public StringBuilder print(StringBuilder stringBuilder) {
        stringBuilder.append(this.number);
        stringBuilder.append(",");
        return this.next.print(stringBuilder);
    }

    /**
     * Berechnet rekursiv momentane Größe der Liste, indem zur Anzahl aller Nachfolger eins addiert wird.
     *
     * @return Größer der Liste, falls head=this
     */
    @Override
    public int size() {
        return this.next.size() + 1;
    }

    /**
     * Gibt false zurück, da wenn ein Objekt der Klasse ListElement in der Liste gespeichert wird, dann ist sie
     * sicher nicht leer
     *
     * @return Wahrheitswert, ob Liste leer ist.
     */
    @Override
    public boolean isEmpty() {
        return false;
    }

    /**
     * Gibt Integer Wert an gesuchtem Index zurück.
     *
     * @param index gesuchter Index
     * @param counter momentaner Index
     * @return Wert am Index index, oder Integer.MIN_VALUE falls index nicht in Liste.
     */
    @Override
    public int get(int index, int counter) {
        if (index == counter) {
            return this.number;
        }
        return this.next.get(index, ++counter);
    }

    /**
     * Gibt den momentanen Index zurück oder sucht rekursiv weiter nach dem Vorkommen der gesuchten number
     *
     * @param number Der Wert, dessen Index zu suchen ist
     * @param counter Der momentane Index
     * @return Der Index des Werts number
     */
    @Override
    public int indexOf(int number, int counter) {
        if (this.number == number) {
            return counter;
        } else {
            return this.next.indexOf(number, ++counter);
        }
    }

    /**
     * Überschreibt bisher gefunden Index, falls Wert an dieser Stelle der zu suchende ist. Sucht danach rekursiv
     * weiter noch dem Vorkommen des Werts number.
     *
     * @param number Der Wert, dessen zuletzt vorkommenden Index zu suchen ist
     * @param counter Der momentane Index
     * @param currentFoundIndex Der bisher gefundene Index an dessen Stelle der Wert number steht
     * @return Index des letzten Vorkommens des Werts number
     */
    @Override
    public int lastIndexOf(int number, int counter, int currentFoundIndex) {
        if (this.number == number) {
            return this.next.lastIndexOf(number, counter + 1, counter);
        } else {
            return this.next.lastIndexOf(number, ++counter, currentFoundIndex);
        }
    }

    /**
     * Löscht sich selbst, indem der Nachfolger zurück gegeben wird, falls der momentane Wert der gesuchte ist.
     *
     * @param number gesuchter zu löschender Wert
     * @return neuer Nachfolger
     */
    @Override
    public ListElementInterface remove(int number) {
        if (this.number == number) {
            return this.next;
        } else {
            this.next = this.next.remove(number);
            return this;
        }
    }

    /**
     * Gibt true zurück, falls momentaner Wert der zu Suchende ist. Anderfalls wird rekursiv weiter nach dem
     * Vorkommen des Werts gesucht.
     *
     * @param number zu suchender Wert
     * @return Wahrheitswert, ob number in Liste enthalten ist
     */
    @Override
    public boolean contains(int number) {
        if (this.number == number) {
            return true;
        } else {
            return this.next.contains(number);
        }
    }
}
