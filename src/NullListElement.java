/**
 * Diese Klasse beschreibt einen Schlussknoten der Liste nach dem Prinzip des Kompositums. Es behandelt die Rekursiv
 * aufgerufenen Methoden aus ListElement und startet den Rücklauf der Rekursion korrekt.
 *
 * @author Cedrico Knoesel
 * @since 18.12.2018
 */
public class NullListElement implements ListElementInterface {
    @Override
    public ListElementInterface add(int number) {
        return new ListElement(number, this);
    }

    @Override
    public StringBuilder print(StringBuilder stringBuilder) {
        int length = stringBuilder.length();
        if (length <= 1) {
            stringBuilder.append("]");
        } else {
            stringBuilder.replace(length - 1, length, "]");
        }
        return stringBuilder;
    }

    /**
     * Startet rekursives Zählen der Elemente
     *
     * @return 0, da ab dieses Element keine Nachfolger hat und selbst nicht zu der Anzahl der Elementen zählt.
     */
    @Override
    public int size() {
        return 0;
    }

    /**
     * Gibt true zurück, da wenn ein Objekt der Klasse NullListElement in der Liste als head gespeichert wird, dann ist
     * sie leer
     *
     * @return Wahrheitswert, ob Liste leer ist.
     */
    @Override
    public boolean isEmpty() {
        return true;
    }

    /**
     * Eingabe ungültig, da index nicht ein Index der momentanen Liste ist. Rückgabewert theoretisch beliebig.
     *
     * @param index gesuchter Index
     * @param counter momentaner Index
     * @return Integer.MIN_VALUE, da index nicht in Liste.
     */
    @Override
    public int get(int index, int counter) {
        return Integer.MIN_VALUE;
    }

    /**
     * Gibt -1 zurück, da number nicht in Liste gefunden wurde.
     *
     * @param number Der Wert, dessen Index zu suchen ist
     * @param counter Der momentane Index
     * @return Der Index des Werts number
     */
    @Override
    public int indexOf(int number, int counter) {
        return -1;
    }

    /**
     * Schickt currentFoundIndex zurück.
     *
     * @param number Der Wert, dessen zuletzt vorkommenden Index zu suchen ist
     * @param counter Der momentane Index
     * @param currentFoundIndex Der bisher gefundene Index an dessen Stelle der Wert number steht
     * @return currentFoundIndex
     */
    @Override
    public int lastIndexOf(int number, int counter, int currentFoundIndex) {
        return currentFoundIndex;
    }

    /**
     * Hier kann nichts mehr gelöscht werden und Rekursion wird zurück gestartet.
     *
     * @param number gesuchter zu löschender Wert
     * @return neuer Nachfolger
     */
    @Override
    public ListElementInterface remove(int number) {
        return this;
    }

    /**
     * Gibt false zurück, da Ende der Liste erreicht wurde und number somit nicht enthalten ist.
     *
     * @param number zu suchender Wert
     * @return Wahrheitswert, ob number in Liste enthalten ist
     */
    @Override
    public boolean contains(int number) {
        return false;
    }
}
